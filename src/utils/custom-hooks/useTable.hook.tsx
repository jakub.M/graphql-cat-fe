import React, { useEffect, useState } from 'react';
import axios from 'axios';
import MaterialTable, { Column } from 'material-table';
import { Button } from '@material-ui/core';

interface props {
  columns : Array<Column<any>>;
  take: number;
}

export const useTable = ({ columns, take } : props) => {
  const [{ data, count }, setData] = useState({data: [], count: 0});
  const [page, setPage] = useState(1);
  const funcName = 'getCatsList';

  useEffect(() => {
    const query = `{
      ${funcName}(take: ${take}, skip: ${(page - 1) * take}){
        data{
          ${columns.map((item: any) => item.field)}
        },
        count
      }
    }`
    const fetchData = async () =>
    await axios.post('http://localhost:3001/graphql', { query })
      .then(res => res.data.data)
      .then(res => setData(res[funcName]))

    fetchData();
  }, [page]);

  const Tab = () =>
    <div>
      <MaterialTable
        title='name'
        columns={columns}
        data={data}
      />
      { page > 1 && <Button onClick={() => setPage(prev => prev - 1)}>prev</Button> }
      <h2>{page}/{Math.ceil(count/take)}</h2>
      { (page < (count / take)) && <Button onClick={() => setPage(prev => prev + 1)}>next</Button> }
    </div>
  
  return { Tab, setData };
}