import React from 'react';
import { Button } from '@material-ui/core';
import Axios from 'axios';

const query =`
mutation {
  createCat(createCatInput: { name:"new Cat", age: 6, test: 0 }){ name, age }
}`

const addItem = async () => await Axios.post('http://localhost:3001/graphql', { query }).then(res => res.data.data.createCat)

export const AddButton = ({ setData, take }: any) =>
<Button
  onClick={() => addItem()
    .then(res => setData((prev: any) => {
      return { ...prev, count: prev.count + 1, data: [...(prev.data.length < take ? [...prev.data, res ] : prev.data)] }
      }))
    }
> Add Item </Button>
