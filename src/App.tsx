import React from 'react';
import './App.css';
import { useTable } from './utils/custom-hooks/useTable.hook';
import { AddButton } from './components/addButton';
import { Button } from '@material-ui/core';


const table = {
  name: 'Cats',
  take: 5,
  columns: [
  { field: 'name', title: 'Name' },
  { field: 'age', title: 'Age' },
  ]
}

export const App: React.FC = () => {
  const { Tab, setData } = useTable(table)
  return (
    <div className="App">
      <header className="App-header">
        {'Example'}
      </header>
      <Tab />
      <AddButton {...{ setData, take: table.take }}/>
    </div>
  );
}
